import {EventType, IEvent} from "../../../../core/api/dto/AlertResponseDto";
import {EventValuesHorizontalText} from "./valueComponents/EventValuesHorizontalText";
import {FunctionComponent} from "react";
import {EventValuesReview} from "./valueComponents/EventValuesReview";
import {EventValuesOnlyOld} from "./valueComponents/EventValuesOnlyOld";
import {EventValuesVerticalText} from "./valueComponents/EventValuesVerticalText";
import {EventValuesImages} from "./valueComponents/EventValuesImages";
import {IEventValuesProps} from "./valueComponents/IEventValuesProps";
import {ValueOf} from "../../../utils/ValueOf";

type EventValuesComponentsMapType = { [key in ValueOf<typeof EventType>]: FunctionComponent<IEventValuesProps> };

const eventComponentsMap: EventValuesComponentsMapType = {
  [EventType.MarkedAsAdult]: EventValuesHorizontalText,
  [EventType.MarkedAsNotAdult]: EventValuesHorizontalText,
  [EventType.ListingNoLongerSuppressed]: EventValuesOnlyOld,
  [EventType.ListingSuppressed]: EventValuesVerticalText,
  [EventType.ProductTitleChanged]: EventValuesVerticalText,
  [EventType.ProductDimensionsChanged]: EventValuesVerticalText,
  [EventType.NumberOfSellersChanged]: EventValuesHorizontalText,
  [EventType.ProductCategoryChanged]: EventValuesVerticalText,
  [EventType.New1StarProductReview]: EventValuesReview,
  [EventType.New2StarProductReview]: EventValuesReview,
  [EventType.New3StarProductReview]: EventValuesReview,
  [EventType.New4StarProductReview]: EventValuesReview,
  [EventType.New5StarProductReview]: EventValuesReview,
  [EventType.BuyBoxWon]: EventValuesHorizontalText,
  [EventType.BuyBoxLost]: EventValuesHorizontalText,
  [EventType.ProductImageChanged]: EventValuesImages,
  [EventType.PriceChanged]: EventValuesHorizontalText,
  [EventType.BuyBoxChanged]: EventValuesHorizontalText,
  [EventType.ProductWidthChanged]: EventValuesVerticalText,
  [EventType.ProductWeightChanged]: EventValuesVerticalText,
  [EventType.ProductLengthChanged]: EventValuesVerticalText,
  [EventType.ProductHeightChanged]: EventValuesVerticalText,
};

export const useEventValueComponent = (event: IEvent): FunctionComponent<IEventValuesProps> => {
  return eventComponentsMap[event.eventType];
};
