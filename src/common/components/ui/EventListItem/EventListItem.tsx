import React, {memo, useMemo} from "react";
import {Image, ImageStyle, Text, TextStyle, View, ViewStyle} from "react-native";
import {useThemeColors} from "../../../theme/colors";
import {styleSheetCreate, styleSheetFlatten} from "../../../utils/styleSheetCreate";
import {FontWeights} from "../../../theme/fonts";
import {CommonStyles} from "../../../CommonStyles";
import {useEventColor} from "./eventColors";
import {IEvent} from "../../../../core/api/dto/AlertResponseDto";
import {DateHelper} from "../../../heplers/DateHelper";
import moment from "moment";

interface IProps {
  children?: any;
  event: IEvent;
}

export const EventListItem = memo((props: IProps) => {
  const eventColor = useEventColor(props.event.eventType);
  const styles = useStyles(eventColor);
  const {children, event} = props;
  const {eventDate, asin, eventTitle, imageUrl, title, createdAt} = event;
  const pastPeriod = useMemo(() => DateHelper.periodPast(moment.unix(createdAt).toDate()), [createdAt]);

  return (
    <View style={styles.shadowContainer}>
      <View style={styles.rootContainer}>
        <View style={styles.colorStrip}/>
        <View style={styles.contentContainer}>
          <Text style={styles.title}>{eventTitle}</Text>
          <View style={styles.descriptionContainer}>
            <View style={styles.imageWrapper}>
              <Image style={styles.productImage} source={{uri: imageUrl}}/>
            </View>
            <View style={styles.descriptionTextSubContainer}>
              <Text style={styles.description} numberOfLines={2}>{title}</Text>
              <Text style={styles.asin}>ASIN: {asin}</Text>
            </View>
          </View>
          <Text style={styles.dateTime}>{eventDate}</Text>
          {children}
          <Text style={styles.period}>{pastPeriod}</Text>
        </View>
      </View>
    </View>
  );
});

const useStyles = (typeColor: string) => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      shadowContainer: styleSheetFlatten([CommonStyles.shadow, {
        alignSelf: "stretch",
        borderRadius: 8,
        marginHorizontal: 8,
        marginVertical: 4,
        backgroundColor: colors.white,
      } as ViewStyle]) as ViewStyle,
      rootContainer: {
        borderRadius: 8,
        overflow: "hidden",
        flexDirection: "row",
        backgroundColor: colors.white,
      } as ViewStyle,
      colorStrip: {
        height: "100%",
        width: 7,
        backgroundColor: typeColor,
      } as ViewStyle,
      contentContainer: {
        flex: 1,
        alignItems: "flex-start",
        justifyContent: "center",
        paddingTop: 12,
        paddingLeft: 6,
        paddingBottom: 6,
        paddingRight: 6,
      } as ViewStyle,
      title: {
        fontSize: 17,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.semibold,
        color: typeColor,
      } as TextStyle,
      descriptionContainer: {
        flexDirection: "row",
        marginTop: 8,
        marginBottom: 6,
      } as ViewStyle,
      imageWrapper: {
        width: 45,
        height: 45,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5,
        overflow: "hidden",
        borderWidth: 1,
        borderColor: colors.paleGrey,
        marginTop: 3,
      } as ViewStyle,
      productImage: {
        width: 45,
        height: 45,
        resizeMode: "contain",
      } as ImageStyle,
      descriptionTextSubContainer: {
        flex: 1,
        marginLeft: 8,
      } as ViewStyle,
      description: {
        fontSize: 13,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.medium,
        color: colors.charcoalGrey,
        marginBottom: 5,
      } as TextStyle,
      asin: {
        fontSize: 11,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.regular,
        color: colors.blueGrey,
      } as TextStyle,
      dateTime: {
        fontSize: 12,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.regular,
        color: colors.battleshipGrey,
      } as TextStyle,
      period: {
        fontSize: 11,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.regular,
        color: colors.blueGrey,
        alignSelf: "flex-end",
        marginTop: 6,
      } as TextStyle,
    }), [colors]);
};
