import React, {useMemo} from "react";
import {Text, TextStyle, View, ViewStyle} from "react-native";
import {useThemeColors} from "../../../../theme/colors";
import {styleSheetCreate} from "../../../../utils/styleSheetCreate";
import {FontWeights} from "../../../../theme/fonts";
import {IEventValuesProps} from "./IEventValuesProps";
import {getRating} from "../../../../constants";

export const EventValuesReview = React.memo((props: IEventValuesProps) => {
  const styles = useStyles();
  const {newValue, paramTitle, oldValue} = props;
  const rating = useMemo(() => getRating(props.eventType), [props.eventType]);

  return (
    <View style={styles.container}>
      <View style={styles.valuesContainer}>
        <View style={styles.subContainer}>
          <Text style={styles.title}>old {paramTitle}: </Text>
          <Text style={styles.description}>{oldValue}</Text>
        </View>
        <View style={styles.subContainer}>
          <Text style={styles.title}>new {paramTitle}: </Text>
          <Text style={styles.description}>{newValue}</Text>
        </View>
      </View>
      <View style={styles.reviewContainer}>
        <Text style={styles.title}>New Review:</Text>
        <View style={styles.reviewDetailContainer}>
          <Text style={styles.stars}>{rating}</Text>
          <Text style={styles.reviewText}>Great for gifts for yourself</Text>
        </View>
      </View>
    </View>
  );
});

const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {} as ViewStyle,
      valuesContainer: {
        flexDirection: "row",
      } as ViewStyle,
      subContainer: {
        width: "50%",
        flexDirection: "row",
        marginTop: 8,
      } as ViewStyle,
      reviewContainer: {
        marginTop: 8,
        marginBottom: 3,
      } as ViewStyle,
      reviewDetailContainer: {
        flexDirection: "row",
        alignItems: "center",
        marginTop: 7,
      } as ViewStyle,
      stars: {
        marginLeft: 16,
        fontSize: 10,
      } as TextStyle,
      title: {
        fontSize: 12,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.regular,
        color: colors.battleshipGrey,
        textTransform: "uppercase",
      } as TextStyle,
      description: {
        fontSize: 13,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.medium,
        color: colors.windowsBlue,
        marginLeft: 6,
      } as TextStyle,
      reviewText: {
        fontSize: 11,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.medium,
        color: colors.windowsBlue,
        marginLeft: 10,
      } as TextStyle,
    }), [colors]);
};
