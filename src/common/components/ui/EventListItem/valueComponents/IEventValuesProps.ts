import {EventType} from "../../../../../core/api/dto/AlertResponseDto";

export interface IEventValuesProps {
  paramTitle: string;
  newValue: string;
  oldValue: string;
  eventType: EventType;
}
