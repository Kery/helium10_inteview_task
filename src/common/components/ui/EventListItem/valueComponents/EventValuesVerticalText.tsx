import React, {useMemo} from "react";
import {Text, TextStyle, View, ViewStyle} from "react-native";
import {useThemeColors} from "../../../../theme/colors";
import {styleSheetCreate} from "../../../../utils/styleSheetCreate";
import {FontWeights} from "../../../../theme/fonts";
import {IEventValuesProps} from "./IEventValuesProps";

export const EventValuesVerticalText = React.memo((props: IEventValuesProps) => {
  const styles = useStyles();
  const {oldValue, paramTitle, newValue} = props;

  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <Text style={styles.title}>old {paramTitle}: </Text>
        <Text style={styles.description}>{oldValue}</Text>
      </View>
      <View style={styles.subContainer}>
        <Text style={styles.title}>new {paramTitle}: </Text>
        <Text style={styles.description}>{newValue}</Text>
      </View>
    </View>
  );
});

const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {} as ViewStyle,
      subContainer: {} as ViewStyle,
      title: {
        fontSize: 12,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.regular,
        color: colors.battleshipGrey,
        textTransform: "uppercase",
        marginTop: 7,
      } as TextStyle,
      description: {
        fontSize: 13,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.medium,
        color: colors.windowsBlue,
        marginLeft: 6,
        marginTop: 3,
      } as TextStyle,
    }), [colors]);
};
