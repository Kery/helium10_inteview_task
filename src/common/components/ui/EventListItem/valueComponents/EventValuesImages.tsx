import React, {useMemo} from "react";
import {Image, ImageStyle, Text, TextStyle, View, ViewStyle} from "react-native";
import {useThemeColors} from "../../../../theme/colors";
import {styleSheetCreate} from "../../../../utils/styleSheetCreate";
import {FontWeights} from "../../../../theme/fonts";
import {IEventValuesProps} from "./IEventValuesProps";

export const EventValuesImages = React.memo((props: IEventValuesProps) => {
  const styles = useStyles();
  const {newValue, oldValue, paramTitle} = props;

  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <Text style={styles.title}>old {paramTitle}</Text>
        <View style={styles.imageWrapper}>
          <Image style={styles.productImage} source={{uri: oldValue}}/>
        </View>
      </View>
      <View style={styles.subContainer}>
        <Text style={styles.title}>new {paramTitle}</Text>
        <View style={styles.imageWrapper}>
          <Image style={styles.productImage} source={{uri: newValue}}/>
        </View>
      </View>
    </View>
  );
});

const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {
        marginVertical: 8,
        flexDirection: "row",
        paddingHorizontal: 42,
        justifyContent: "space-between",
        alignSelf: "stretch",
      } as ViewStyle,
      subContainer: {
        alignItems: "center",
      } as ViewStyle,
      title: {
        fontSize: 12,
        lineHeight: 18,
        letterSpacing: 0.3,
        fontWeight: FontWeights.regular,
        color: colors.battleshipGrey,
        textTransform: "uppercase",
        marginTop: 7,
      } as TextStyle,
      imageWrapper: {
        width: 75,
        height: 75,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 5,
        overflow: "hidden",
        borderWidth: 1,
        borderColor: colors.paleGrey,
        marginTop: 8,
      } as ViewStyle,
      productImage: {
        width: 75,
        height: 75,
        resizeMode: "contain",
      } as ImageStyle,
    }), [colors]);
};
