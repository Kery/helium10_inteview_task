import React, {useMemo} from "react";
import {IEvent} from "../../../../core/api/dto/AlertResponseDto";
import {useEventValueComponent} from "./eventComponentsMap";
import {useEventValueComponentPropsMapper} from "./eventPropsMapper";

interface IProps {
  event: IEvent;
}

export const EventValues = React.memo((props: IProps) => {
  const Component = useMemo(() => useEventValueComponent(props.event), [props]);
  const eventValuesPropsMapper = useMemo(() => useEventValueComponentPropsMapper(props.event.eventType), [props]);

  return <Component {...eventValuesPropsMapper(props.event)}/>;
});
