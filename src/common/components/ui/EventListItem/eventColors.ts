import {EventType} from "../../../../core/api/dto/AlertResponseDto";
import {ValueOf} from "../../../utils/ValueOf";
import {Colors} from "../../../theme/colors";

type EventColorsType = { [key in ValueOf<typeof EventType>]: string };
const eventColors: EventColorsType = {
  [EventType.MarkedAsAdult]: Colors.rouge,
  [EventType.MarkedAsNotAdult]: Colors.rouge,
  [EventType.ListingNoLongerSuppressed]: Colors.softBlue,
  [EventType.ListingSuppressed]: Colors.softBlue,
  [EventType.ProductTitleChanged]: Colors.blueGrey,
  [EventType.ProductDimensionsChanged]: Colors.eggplant,
  [EventType.NumberOfSellersChanged]: Colors.desert,
  [EventType.ProductCategoryChanged]: Colors.salmon,
  [EventType.New1StarProductReview]: Colors.yellowOrange,
  [EventType.New2StarProductReview]: Colors.yellowOrange,
  [EventType.New3StarProductReview]: Colors.yellowOrange,
  [EventType.New4StarProductReview]: Colors.yellowOrange,
  [EventType.New5StarProductReview]: Colors.yellowOrange,
  [EventType.BuyBoxWon]: Colors.dustyOrange,
  [EventType.BuyBoxLost]: Colors.dustyOrange,
  [EventType.ProductImageChanged]: Colors.ocean,
  [EventType.PriceChanged]: Colors.niceBlue,
  [EventType.BuyBoxChanged]: Colors.dustyOrange,
  [EventType.ProductWidthChanged]: Colors.eggplant,
  [EventType.ProductWeightChanged]: Colors.eggplant,
  [EventType.ProductLengthChanged]: Colors.eggplant,
  [EventType.ProductHeightChanged]: Colors.eggplant,
};

export const useEventColor = (eventType: EventType): string => {
  return eventColors[eventType];
};
