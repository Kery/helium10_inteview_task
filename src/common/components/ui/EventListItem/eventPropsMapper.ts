import {IEventValuesProps} from "./valueComponents/IEventValuesProps";
import {
  EventType,
  ICategory,
  IDimension,
  IEvent,
  IMeasurement,
  IPrice,
  ISeller,
} from "../../../../core/api/dto/AlertResponseDto";
import {localization} from "../../../localization/localization";
import {ValueOf} from "../../../utils/ValueOf";

type mapperFunc = (event: IEvent) => IEventValuesProps;
type EventValuesPropsMapperType = { [key in ValueOf<typeof EventType>]: mapperFunc };

const eventPropsMapper: EventValuesPropsMapperType = {
  [EventType.MarkedAsAdult]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.MarkedAsNotAdult]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.ListingNoLongerSuppressed]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.ListingSuppressed]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.ProductTitleChanged]: getDefaultMapper(localization.common.alertLabels.title),
  [EventType.ProductDimensionsChanged]: getDimensionsMapper(localization.common.alertLabels.dimensions),
  [EventType.NumberOfSellersChanged]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.ProductCategoryChanged]: getCategoryMapper(localization.common.alertLabels.category),
  [EventType.New1StarProductReview]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.New2StarProductReview]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.New3StarProductReview]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.New4StarProductReview]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.New5StarProductReview]: getDefaultMapper(localization.common.alertLabels.value),
  [EventType.BuyBoxWon]: getSellerMapper(localization.common.alertLabels.seller),
  [EventType.BuyBoxLost]: getSellerMapper(localization.common.alertLabels.seller),
  [EventType.ProductImageChanged]: getDefaultMapper(localization.common.alertLabels.image),
  [EventType.PriceChanged]: getPriceMapper(localization.common.alertLabels.price),
  [EventType.ProductWidthChanged]: getMeasurementMapper(localization.common.alertLabels.width),
  [EventType.BuyBoxChanged]: getSellerMapper(localization.common.alertLabels.value),
  [EventType.ProductWeightChanged]: getMeasurementMapper(localization.common.alertLabels.weight),
  [EventType.ProductLengthChanged]: getMeasurementMapper(localization.common.alertLabels.length),
  [EventType.ProductHeightChanged]: getMeasurementMapper(localization.common.alertLabels.height),
};

function getDefaultMapper(paramTitle: string): (event: IEvent) => IEventValuesProps {
  return (event: IEvent) => ({
    newValue: `${event.newValue}`,
    oldValue: `${event.oldValue}`,
    paramTitle: paramTitle,
    eventType: event.eventType,
  });
}

function getDimensionsMapper(paramTitle: string): (event: IEvent) => IEventValuesProps {
  return (event: IEvent) => {
    const oldValues = event.oldValue as IDimension[];
    const newValues = event.newValue as IDimension[];

    return {
      newValue: newValues.map(({params, units, value}: IDimension) => `${params}: ${value} ${units}`).join(`\n`),
      oldValue: oldValues.map(({params, units, value}: IDimension) => `${params}: ${value} ${units}`).join(`\n`),
      paramTitle: paramTitle,
      eventType: event.eventType,
    };
  };
}

function getCategoryMapper(paramTitle: string): (event: IEvent) => IEventValuesProps {
  return (event: IEvent) => {
    const oldValues = event.oldValue as ICategory[];
    const newValues = event.newValue as ICategory[];

    return {
      newValue: newValues.map(({title}: ICategory) => title).join(`, `),
      oldValue: oldValues.map(({title}: ICategory) => title).join(`, `),
      paramTitle: paramTitle,
      eventType: event.eventType,
    };
  };
}

function getSellerMapper(paramTitle: string): (event: IEvent) => IEventValuesProps {
  return (event: IEvent) => {
    const oldValue = event.oldValue as ISeller;
    const newValue = event.newValue as ISeller;

    return {
      newValue: newValue.sellerName,
      oldValue: oldValue.sellerName,
      paramTitle: paramTitle,
      eventType: event.eventType,
    };
  };
}

function getPriceMapper(paramTitle: string): (event: IEvent) => IEventValuesProps {
  return (event: IEvent) => {
    const oldValue = event.oldValue as IPrice;
    const newValue = event.newValue as IPrice;

    return {
      newValue: `${newValue.price} ${newValue.currency}`,
      oldValue: `${oldValue.price} ${oldValue.currency}`,
      paramTitle: paramTitle,
      eventType: event.eventType,
    };
  };
}

function getMeasurementMapper(paramTitle: string): (event: IEvent) => IEventValuesProps {
  return (event: IEvent) => {
    const oldValue = event.oldValue as IMeasurement;
    const newValue = event.newValue as IMeasurement;

    return {
      newValue: `${newValue.value} ${newValue.measurement}`,
      oldValue: `${oldValue.value} ${oldValue.measurement}`,
      paramTitle: paramTitle,
      eventType: event.eventType,
    };
  };
}

export const useEventValueComponentPropsMapper = (eventType: EventType): mapperFunc => {
  return eventPropsMapper[eventType];
};
