import React, {PureComponent} from "react";
import {Text, TextStyle, TouchableOpacity, View, ViewStyle} from "react-native";
import {localization} from "../../localization/localization";
import {styleSheetCreate} from "../../utils/styleSheetCreate";
import {Colors} from "../../theme/colors";
import {FontWeights} from "../../theme/fonts";

interface IProps {
  onPress: () => void;
  errorText?: string | null;
}

export class TryAgain extends PureComponent<IProps, IEmpty> {
  render(): JSX.Element {
    return (
      <View style={styles.container}>
        <Text style={styles.text}>{this.props.errorText}</Text>
        <TouchableOpacity onPress={this.props.onPress}>
          <Text style={styles.textTryMore}>{localization.errors.tryAgain}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = styleSheetCreate({
  container: {
    flex: 1,
    justifyContent: "center",
  } as ViewStyle,
  text: {
    color: Colors.darkBlue,
    fontWeight: FontWeights.regular,
    textAlign: "center",
    fontSize: 16,
  } as TextStyle,
  textTryMore: {
    color: Colors.blue,
    fontWeight: FontWeights.regular,
    textAlign: "center",
    textDecorationLine: "underline",
    fontSize: 16,
  } as TextStyle,
});
