import {useThemeColors} from "../../theme/colors";
import React, {useMemo} from "react";
import {styleSheetCreate} from "../../utils/styleSheetCreate";
import {View, ViewStyle} from "react-native";

export const HeaderLeft = () => <View style={useStyles().container}/>;
const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {
        width: 87,
      } as ViewStyle,
    }), [colors]);
};
