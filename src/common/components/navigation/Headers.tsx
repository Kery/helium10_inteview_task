import {ImageURISource, TextStyle, View, ViewStyle} from "react-native";
import React from "react";
import {StackNavigationOptions} from "@react-navigation/stack/lib/typescript/src/types";
import {styleSheetCreate} from "../../utils/styleSheetCreate";
import {Colors} from "../../theme/colors";
import {FontWeights} from "../../theme/fonts";

export const NavigationHeaders = {
  plainHeader(params: {
    title: string;
    showBackButton?: boolean;
    headerStyle?: ViewStyle;
    backIcon?: ImageURISource;
    RightHeaderComponent?: React.ComponentType;
    LeftHeaderComponent?: React.ComponentType;
  }): StackNavigationOptions {
    const {title, RightHeaderComponent, LeftHeaderComponent} = params;
    const rightComponent = RightHeaderComponent ? <RightHeaderComponent/> : <View/>;
    const leftComponent = LeftHeaderComponent ? <LeftHeaderComponent/> : <View/>;

    return ({
      headerTitle: title,
      headerTitleStyle: styles.headerTitleStyle,
      headerRight: () => rightComponent,
      headerLeft: () => leftComponent,
      headerStyle: styles.header,
      headerTitleAllowFontScaling: false,
    });
  },
};
const styles = styleSheetCreate({
  header: {
    borderBottomWidth: 0,
    borderBottomColor: Colors.transparent,
    backgroundColor: Colors.darkBlue,
    elevation: 0,
    borderRadius: 8,
  } as ViewStyle,
  headerTitleStyle: {
    alignSelf: "center",
    textAlign: "center",
    fontSize: 17,
    fontWeight: FontWeights.light,
    letterSpacing: -0.41,
    color: Colors.white,
    elevation: 0,
  } as TextStyle,
});
