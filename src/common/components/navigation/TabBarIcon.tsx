import React, {useMemo} from "react";
import {Image, ImageStyle, ImageURISource} from "react-native";
import {useThemeColors} from "../../theme/colors";
import {styleSheetCreate} from "../../utils/styleSheetCreate";

interface IProps {
  source: ImageURISource;
  isFocused: boolean;
}

export const TabBarIcon = React.memo(({isFocused, source}: IProps): JSX.Element => {
  const styles = useStyles();

  return (
    <Image
      style={isFocused ? styles.activeImage : styles.image}
      source={source}
      defaultSource={source}

    />);
});

const preStyles = {
  icon: {
    height: 22,
    width: 28,
    resizeMode: "contain",
  } as ImageStyle,
};

const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      activeImage: {
        ...preStyles.icon,
      } as ImageStyle,
      image: {
        ...preStyles.icon,
        opacity: 0.5,
      } as ImageStyle,
    }), [colors]);
};
