import {useThemeColors} from "../../theme/colors";
import React, {useMemo} from "react";
import {styleSheetCreate} from "../../utils/styleSheetCreate";
import {Text, TextStyle, TouchableOpacity, View, ViewStyle} from "react-native";
import {FontWeights} from "../../theme/fonts";
import {localization} from "../../localization/localization";
import {showInDevAlert} from "../../heplers/showInDevAlert";
import {hitSlop} from "../../theme/common";

export const HeaderRight = () => {
  const styles = useStyles();

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.leftButton} onPress={showInDevAlert} activeOpacity={0.8} hitSlop={hitSlop}>
        <View style={styles.longLine}/>
        <View style={styles.middleLine}/>
        <View style={styles.shortLine}/>
      </TouchableOpacity>
      <TouchableOpacity style={styles.regionButton} onPress={showInDevAlert} activeOpacity={0.8} hitSlop={hitSlop}>
        <Text style={styles.regionLabel}>{localization.common.regionEU}</Text>
      </TouchableOpacity>
    </View>
  );
};
const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {
        width: 87,
        flexDirection: "row",
        justifyContent: "flex-end",
        paddingRight: 12,
      } as ViewStyle,
      leftButton: {
        alignItems: "flex-start",
        justifyContent: "center",
        width: 24,
        height: 24,
        paddingHorizontal: 3,
        marginRight: 21,
      } as ViewStyle,
      longLine: {
        width: 18,
        height: 2,
        backgroundColor: colors.white,
        borderRadius: 1,
      } as ViewStyle,
      middleLine: {
        width: 10,
        height: 2,
        backgroundColor: colors.white,
        borderRadius: 1,
        marginVertical: 3,
      } as ViewStyle,
      shortLine: {
        width: 4,
        height: 2,
        backgroundColor: colors.white,
        borderRadius: 1,
      } as ViewStyle,
      regionButton: {
        width: 30,
        height: 30,
        borderRadius: 15,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.blue,
      } as ViewStyle,
      regionLabel: {
        color: colors.white,
        fontSize: 15,
        fontWeight: FontWeights.medium,
      } as TextStyle,
    }), [colors]);
};
