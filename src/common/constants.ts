import {EventType} from "../core/api/dto/AlertResponseDto";

const reviewRating: EventType[] = [
  EventType.New1StarProductReview,
  EventType.New2StarProductReview,
  EventType.New3StarProductReview,
  EventType.New4StarProductReview,
  EventType.New5StarProductReview,
];

export const getRating = (eventType: EventType): string => {
  return Array.from(Array(reviewRating.indexOf(eventType) + 1)).map(i => "⭐").join("");
};
