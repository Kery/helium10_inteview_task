import {Colors} from "./theme/colors";
import {styleSheetCreate} from "./utils/styleSheetCreate";
import {ViewStyle} from "react-native";

export const CommonStyles = styleSheetCreate({
  flex1: {
    flex: 1,
  } as ViewStyle,
  shadow: {
    shadowColor: Colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    // TODO: check shadow, zeplin shadows looks different
    shadowOpacity: 0.25, //0.5
    shadowRadius: 2.5,  //6
    elevation: 4,
  },
});
