import {ImageURISource} from "react-native";

export class ImageResources {
  static readonly hijackerAlert: ImageURISource = require("../../resources/images/hijackerAlert.png");
  static readonly profits: ImageURISource = require("../../resources/images/profits.png");

}
