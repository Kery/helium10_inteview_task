import {ColorSchemeName, useColorScheme} from "react-native-appearance";

export const Colors = {
  black: "#000000",
  white: "#FFFFFF",
  blue: "#0081FF",
  darkBlue: "#162343",
  transparent: "transparent",
  charcoalGrey: "#343a40",
  blueGrey: "#8ca0b3",
  windowsBlue: "#337ab7",
  battleshipGrey: "#6c757d",
  paleGrey: "#ebf2f7",
  salmon: "#fe6b6b",
  ocean: "#037f8c",
  desert: "#cecb3d",
  dustyOrange: "#f28a2e",
  niceBlue: "#0d50a6",
  eggplant: "#400b38",
  yellowOrange: "#f3b302",
  softBlue: "#5b72ef",
  rouge: "#bf1737",

};

export const LightThemeColors = {
  background: "#fff",
};
export const DarkThemeColors = {
  background: "#fff",
};

const allColors = Object.assign({}, Colors, LightThemeColors, DarkThemeColors);

export function useThemeColors(): ReturnType<() => typeof allColors> {
  const scheme: ColorSchemeName = useColorScheme();

  return Object.assign({}, Colors, scheme === "dark" ? DarkThemeColors : LightThemeColors);
}

export function useThemeStatusBar(): string {
  const scheme: ColorSchemeName = useColorScheme();

  return scheme === "dark" ? "light-content" : "dark-content";
}
