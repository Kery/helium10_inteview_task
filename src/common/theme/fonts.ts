export class Fonts {
  //custom fonts
}

export class FontWeights {
  static light = "300";
  static regular = "400";
  static medium = "500";
  static semibold = "600";
  static bold = "bold";
}
