import React, {ReactElement, useMemo} from "react";
import {Text, View, ViewStyle} from "react-native";
import {styleSheetCreate} from "../../common/utils/styleSheetCreate";
import {useThemeColors} from "../theme/colors";
import {localization} from "../localization/localization";

export const InDevelop = (): ReactElement => {
  const styles = useStyles();

  return (
    <View style={styles.container}>
      <Text>{localization.empty.inDevMessage}</Text>
    </View>
  );
};

const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: colors.background,
      } as ViewStyle,
    }), [colors]);
};
