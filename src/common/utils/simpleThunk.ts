import {ThunkAction} from "redux-thunk";
import {Action} from "redux";
import {IAppState} from "../../core/store/app/appState";

export type SimpleThunk<T = Promise<void>> = ThunkAction<T, IAppState, Error, Action>;
