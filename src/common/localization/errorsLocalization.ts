export const errorsLocalization = {
    "en": {
        "unknownError": "An error occurred, try again later",
        "listErrorTitle": "Failed to load data.\nPlease check your Internet connection",
        "tryAgain": "Try again",
        "mandatoryField": "Required field",
        "noInternetConnection": "No server connection. Check your Internet connection",
        "error": "Error: ",

        "failedToConnect": "Failed to connect to the server.",
        "checkConnection": "Check your Internet connection and try again later.",
    },
};
