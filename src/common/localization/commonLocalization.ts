export const commonLocalization = {
  "en": {
    "ok": "ok",
    "regionEU": "EU",
    "alertLabels": {
      "value": "value",
      "title": "title",
      "dimensions": "dimensions",
      "category": "category",
      "seller": "seller",
      "image": "image",
      "price": "price",
      "width": "width",
      "weight": "weight",
      "length": "length",
      "height": "height",
    },
  },
};
