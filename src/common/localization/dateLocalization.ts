export const dateLocalization = {
    "en": {
        "today": "today",
        "tomorrow": "tomorrow",
        "yesterday": "yesterday",
        "lastYear": "last year",
        "yearsAgo": "years ago",
        "lastMonth": "last month",
        "monthsAgo": "months ago",
        "lastWeek": "last week",
        "weeksAgo": "weeks ago",
        "daysAgo": "days ago",
        "dayAfterTomorrow": "day after tomorrow",
        "from": "from",
        "to": "to",
    },
};
