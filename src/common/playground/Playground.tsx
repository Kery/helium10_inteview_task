import React, {PureComponent} from "react";
import {ScrollView, Text, TextStyle, View, ViewStyle} from "react-native";
import {styleSheetCreate} from "../utils/styleSheetCreate";
import {Colors} from "../theme/colors";

interface IState {
}

export class Playground extends PureComponent<IEmpty, IState> {
  constructor(props: IEmpty) {
    super(props);
    this.state = {};
  }

  render(): JSX.Element {
    return (
      <ScrollView style={styles.mainContainer}>
        {this.renderContentInfo("MainButton (Action)")}
        <Text>Put example component here</Text>
      </ScrollView>
    );
  }

  private renderContentInfo = (title: string): JSX.Element => {
    return (
      <React.Fragment>
        <View style={styles.contentContainer}/>
        <Text style={styles.title}>{title}</Text>
      </React.Fragment>
    );
  };
}

const styles = styleSheetCreate({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.white,
  } as ViewStyle,
  contentContainer: {
    marginTop: 30,
    borderTopWidth: 1,
    borderTopColor: "purple",
  } as ViewStyle,
  title: {
    color: Colors.black,
    textAlign: "center",
    marginVertical: 10,
  } as TextStyle,
});
