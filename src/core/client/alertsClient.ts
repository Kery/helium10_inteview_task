import {eventsMockup} from "./mockup/eventsMockup";
import {IEventsResponseDto} from "../api/dto/AlertResponseDto";
import {localization} from "../../common/localization/localization";

export class AlertsClient {
  async getEvents(page: number, pageSize: number): Promise<IEventsResponseDto> {
    try {
      if (Math.round(Math.random() * 10) === 1 && page == 1) {
        throw new Error(localization.errors.unknownError);
      }
      const response = {...eventsMockup};
      response.results.data = eventsMockup.results.data.map(event => ({...event, id: event.id + page + 1000}));
      if (page < 4) {
        return Promise.resolve(response);
      } else {
        const emptyResponse = {...eventsMockup};
        emptyResponse.results.data = [];

        return Promise.resolve(emptyResponse);
      }

    } catch (e) {
      throw e;
    }
  }
}
