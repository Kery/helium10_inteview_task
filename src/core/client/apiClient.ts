import {AlertsClient} from "./alertsClient";

class ApiClient {
  alerts = new AlertsClient();
}

export const apiClient = new ApiClient();
