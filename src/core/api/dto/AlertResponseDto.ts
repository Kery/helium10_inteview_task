export type EventValueType = string | number | IMeasurement | IPrice | ISeller | IDimension[] | ICategory[];

export interface IEventsResponseDto {
  results: {
    data: IEvent[];
    meta: {
      cursor: string;
    }
  };
}

export interface IEvent {
  id: number;
  newValue: EventValueType;
  oldValue: EventValueType;
  createdAt: number;
  eventDate: string;
  eventType: EventType;
  eventTitle: string;
  asin: string;
  sellerSku: string;
  title: string;
  imageUrl: string;
}

export enum EventType {
  ProductTitleChanged = 1,
  ProductCategoryChanged = 2,
  ProductImageChanged = 4,
  NumberOfSellersChanged = 5,
  BuyBoxChanged = 6,
  PriceChanged = 7,
  ProductWidthChanged = 8,
  ProductHeightChanged = 9,
  ProductLengthChanged = 10,
  ProductWeightChanged = 11,
  BuyBoxLost = 14,
  BuyBoxWon = 15,
  ProductDimensionsChanged = 16,
  New5StarProductReview = 17,
  New4StarProductReview = 18,
  New3StarProductReview = 19,
  New2StarProductReview = 20,
  New1StarProductReview = 21,
  ListingSuppressed = 23,
  ListingNoLongerSuppressed = 24,
  MarkedAsAdult = 25,
  MarkedAsNotAdult = 26,
}

export interface IMeasurement {
  value: number;
  measurement: string;
}

export interface IDimension {
  params: string;
  value: number;
  units: string;
}

export interface ICategory {
  id?: string | number;
  title: string | number;
  fullTitle: string | number;
}

export interface ISeller {
  sellerId: string;
  sellerName: string;
  sellerUrl: string;
}

export interface IPrice {
  price: number;
  currency: string;
}
