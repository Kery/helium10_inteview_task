import {Reducer} from "redux";
import {IAppState} from "./appState";
import {Reducers} from "../Reducers";
import {systemReducer} from "../system/systemReducer";
import {alertsReducer} from "../../../modules/Alerts/alertsReducer";

export function createMainReducer(combineMethod: (reducers: any) => Reducer<IAppState>): Reducer<IAppState> {
  const reducers: Reducers<IAppState> = {
    system: systemReducer,
    alerts: alertsReducer,
  };

  return combineMethod(reducers);
}
