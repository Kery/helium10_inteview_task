import {ISystemState, SystemInitialState} from "../system/systemState";
import {alertsInitialState, IAlertsState} from "../../../modules/Alerts/IAlertsState";

export interface IAppState {
  system: ISystemState;
  alerts: IAlertsState;
}

export function getAppInitialState(): IAppState {
  return {
    system: SystemInitialState,
    alerts: alertsInitialState,
  };
}
