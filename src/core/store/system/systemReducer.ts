import {reducerWithInitialState} from "typescript-fsa-reducers";
import {IAppState} from "../app/appState";
import {CoreActions} from "../coreActions";
import {ISystemState, SystemInitialState} from "./systemState";
import {newState} from "../../../common/utils/newState";

function rehydrateHandler(state: ISystemState, rehydratedState: IAppState): ISystemState {
  return newState(rehydratedState.system || state, {});
}

export const systemReducer = reducerWithInitialState(SystemInitialState)
  .case(CoreActions.rehydrate, rehydrateHandler)
  .build();
