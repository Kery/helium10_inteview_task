export interface ISystemState {
  buildNumber: number;
}

export const SystemInitialState: ISystemState = {
  buildNumber: 1,
};
