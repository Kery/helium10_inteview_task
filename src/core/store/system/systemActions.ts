import {actionCreator} from "../actionCreator";

export class SystemActions {
  static setPhoneNumber = actionCreator<string>("System/SET_PHONE_NUMBER");
  static setStat = actionCreator<number>("System/SET_STAT");
}
