import {ReduxLoggerOptions} from "redux-logger";

export interface IAppSettings {
  appName: string;
  environment: "Development" | "Test" | "Staging" | "Production";
  serverUrl: string;
  version: string;
  build: number;
  showVersion: boolean;
  devOptions: IDevOptions;
}

interface IDevOptions {
  purgeStateOnStart: boolean;
  showAllComponentsOnStart: boolean;
  reduxLoggerWhiteList?: string[];
  disableReduxLogger: boolean;
  reduxLogger?: ReduxLoggerOptions;
}
