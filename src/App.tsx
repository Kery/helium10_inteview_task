// tslint:disable:no-import-side-effect
import "react-native-gesture-handler";
import React, {Component} from "react";
import {appSettingsProvider} from "./core/settings";
import {configureStore, MigrateStoreMode} from "./core/store/configureStore";
import {IAppState} from "./core/store/app/appState";
import {Store} from "redux";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import {StatusBar, View} from "react-native";
import {NavigationContainer} from "@react-navigation/native";
import {Colors} from "./common/theme/colors";
import {MainTabsNavigator} from "./navigation/navigators/MainTabsNavigator";

export class App extends Component {

  constructor(props: {}) {
    super(props);
    this.createStore(appSettingsProvider.settings.devOptions.purgeStateOnStart
      ? MigrateStoreMode.purge
      : MigrateStoreMode.none,
    );
  }

  render(): JSX.Element {

    return (
      <Provider store={this.store}>
        <PersistGate loading={<View/>} persistor={this.persistor}>
          <StatusBar backgroundColor={Colors.transparent} translucent={true} barStyle={"light-content"}/>
          <NavigationContainer>
            <MainTabsNavigator/>
          </NavigationContainer>
        </PersistGate>
      </Provider>
    );
  }

  private createStore(mode: MigrateStoreMode): void {
    const {store, persistor} = configureStore(this.onStoreConfigured, {migrateMode: mode});
    this.store = store;
    this.persistor = persistor;
  }

  private onStoreConfigured(): void {
    // const store = this.store;
    // const state = store.getState();
  }

  private store: Store<IAppState>;
  private persistor: any;
}
