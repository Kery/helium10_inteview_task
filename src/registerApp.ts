import {appSettingsProvider} from "./core/settings";
import {AppRegistry} from "react-native";
import {App} from "./App";
import {Playground} from "./common/playground/Playground";

export function registerApp(): void {
  const rootComponent = appSettingsProvider.settings.devOptions.showAllComponentsOnStart ? Playground : App;
  AppRegistry.registerComponent(appSettingsProvider.settings.appName, () => rootComponent);
}
