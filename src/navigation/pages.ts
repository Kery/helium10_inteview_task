export enum MainTabs {
  Profits = "Profits",
  Alerts = "Alerts",
  Account = "  Account",
}

export enum NavigationPages {
  AlertsList = "AlertsList",
}
