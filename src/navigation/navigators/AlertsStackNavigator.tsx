import React from "react";
import {NavigationPages} from "../pages";
import {Alerts} from "../../modules/Alerts/Alerts";
import {createStackNavigator} from "@react-navigation/stack";
import {localization} from "../../common/localization/localization";
import {NavigationHeaders} from "../../common/components/navigation/Headers";
import {HeaderRight} from "../../common/components/navigation/HeaderRight";
import {HeaderLeft} from "../../common/components/navigation/HeadeLeft";

const Stack = createStackNavigator();
export const AlertsStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name={NavigationPages.AlertsList} component={Alerts} options={NavigationHeaders.plainHeader(
        {
          title: localization.pages.alerts,
          showBackButton: false,
          RightHeaderComponent: HeaderRight,
          LeftHeaderComponent: HeaderLeft,
        })}/>
    </Stack.Navigator>
  );
};
