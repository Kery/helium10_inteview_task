import React from "react";
import {MainTabs} from "../pages";
import {createBottomTabNavigator} from "@react-navigation/bottom-tabs";
import {InDevelop} from "../../common/inDevelop/InDevelop";
import {ImageURISource, TextStyle, ViewStyle} from "react-native";
import {TabBarIcon} from "../../common/components/navigation/TabBarIcon";
import {BottomTabBarOptions, BottomTabNavigationOptions} from "@react-navigation/bottom-tabs/lib/typescript/src/types";
import {localization} from "../../common/localization/localization";
import {ImageResources} from "../../common/ImageResources.g";
import {Colors} from "../../common/theme/colors";
import {tabBarHeight} from "../../common/theme/common";
import {AlertsStackNavigator} from "./AlertsStackNavigator";
import {FontWeights} from "../../common/theme/fonts";

const Tab = createBottomTabNavigator();
export const MainTabsNavigator = () => {
  return (
    <Tab.Navigator tabBarOptions={config} lazy={false} initialRouteName={MainTabs.Alerts}>
      <Tab.Screen
        name={MainTabs.Profits}
        component={InDevelop}
        options={screenOptions[MainTabs.Profits]}
      />
      <Tab.Screen
        name={MainTabs.Alerts}
        component={AlertsStackNavigator}
        options={screenOptions[MainTabs.Alerts]}
      />
      <Tab.Screen
        name={MainTabs.Account}
        component={InDevelop}
        options={screenOptions[MainTabs.Account]}
      />
    </Tab.Navigator>
  );
};

const config: BottomTabBarOptions = {
  labelStyle: {
    fontSize: 10,
    paddingBottom: 8,
    fontWeight: FontWeights.medium,
    letterSpacing: -.24,
  } as TextStyle,
  activeTintColor: Colors.white,
  inactiveTintColor: `${Colors.white}7f`,
  style: {
    height: tabBarHeight,
    elevation: 0,
    shadowOffset: undefined,
    shadowRadius: 0,
    backgroundColor: Colors.darkBlue,
    borderTopWidth: 0,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    marginTop: -8,
  } as ViewStyle,
  tabStyle: {
    paddingTop: 4,
    justifyContent: "center",
  },
  keyboardHidesTabBar: true,
};

const renderIcon = (source: ImageURISource): (params: { focused: boolean }) => JSX.Element => (
  (params): JSX.Element => <TabBarIcon source={source} isFocused={params.focused}/>
);

const screenOptions: { [key in MainTabs]: () => BottomTabNavigationOptions } = {
  [MainTabs.Profits]: (): BottomTabNavigationOptions => ({
    title: localization.pages.profits,
    tabBarIcon: renderIcon(ImageResources.profits),
  }),
  [MainTabs.Alerts]: (): BottomTabNavigationOptions => ({
    title: localization.pages.alerts,
    tabBarIcon: renderIcon(ImageResources.hijackerAlert),
  }),
  [MainTabs.Account]: (): BottomTabNavigationOptions => ({
    title: localization.pages.account,
    tabBarIcon: renderIcon(ImageResources.profits),
  }),
};
