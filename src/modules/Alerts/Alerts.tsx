import React, {ReactElement, useCallback, useEffect, useMemo} from "react";
import {ListRenderItemInfo, View, ViewStyle} from "react-native";
import {LoadState} from "../../common/loadState";
import {useDispatch, useSelector} from "react-redux";
import {AlertsActionsAsync} from "./alertsActionsAsync";
import {FlatListWrapper} from "../../common/components/service/FlatListWrapper";
import {IAppState} from "../../core/store/app/appState";
import {EventListItem} from "../../common/components/ui/EventListItem/EventListItem";
import {IEvent} from "../../core/api/dto/AlertResponseDto";
import {EventValues} from "../../common/components/ui/EventListItem/EventValues";
import {useThemeColors} from "../../common/theme/colors";
import {styleSheetCreate} from "../../common/utils/styleSheetCreate";

export const Alerts = (): ReactElement => {
  const styles = useStyles();
  const dispatch = useDispatch();

  const {loadState, content: events} = useSelector((state: IAppState) => state.alerts.events);

  const refresh = useCallback(() => dispatch(AlertsActionsAsync.getEvents(LoadState.pullToRefresh)), [dispatch]);
  const loadMore = useCallback(() => dispatch(AlertsActionsAsync.getEvents(LoadState.loadingMore)), [dispatch]);
  const tryAgain = useCallback(() => dispatch(AlertsActionsAsync.getEvents(LoadState.firstLoad)), [dispatch]);

  const renderHeader = useCallback(() => <View style={styles.header}/>, []);
  const renderFooter = useCallback(() => <View style={styles.footer}/>, []);
  const renderItem = useCallback(({item}: ListRenderItemInfo<IEvent>) => (
    <EventListItem event={item}>
      <EventValues event={item}/>
    </EventListItem>
  ), []);

  useEffect(() => {
    dispatch(AlertsActionsAsync.getEvents(LoadState.firstLoad));
  }, []);

  return (
    <FlatListWrapper
      data={events}
      renderItem={renderItem}
      loadState={loadState}
      ListHeaderComponent={renderHeader}
      ListFooterComponent={renderFooter}
      tryAgain={tryAgain}
      loadMore={loadMore}
      onRefresh={refresh}
      showsVerticalScrollIndicator={true}
    />
  );
};

const useStyles = () => {
  const colors = useThemeColors();

  return useMemo(() =>
    styleSheetCreate({
      container: {
        flex: 1,
        backgroundColor: colors.background,
      } as ViewStyle,
      header: {height: 4} as ViewStyle,
      footer: {height: 12} as ViewStyle,
    }), [colors]);
};
