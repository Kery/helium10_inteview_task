import {ILoadable, LoadState} from "../../common/loadState";
import {IEvent} from "../../core/api/dto/AlertResponseDto";

export interface IAlertsState {
  events: ILoadable<IEvent[]>;
}

export const alertsInitialState: IAlertsState = {
  events: {
    content: [],
    loadState: LoadState.needLoad,
  },
};
