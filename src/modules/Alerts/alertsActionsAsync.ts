import {LoadState} from "../../common/loadState";
import {SimpleThunk} from "../../common/utils/simpleThunk";
import {IAppState} from "../../core/store/app/appState";
import {Dispatch} from "react";
import {calculatePage, DEFAULT_PAGE_SIZE} from "../../common/heplers/calculatePage";
import {apiClient} from "../../core/client/apiClient";
import {AlertsActions} from "./alertsActions";

export class AlertsActionsAsync {
  static getEvents(loadState: LoadState): SimpleThunk {
    return async (dispatch: Dispatch<any>, getState: () => IAppState): Promise<void> => {
      dispatch(AlertsActions.getEvents.started(loadState));
      const count = getState().alerts.events.content.length;
      const page = loadState == LoadState.refreshing || loadState == LoadState.firstLoad || loadState == LoadState.pullToRefresh ? 1 :
        calculatePage(count, DEFAULT_PAGE_SIZE);
      try {
        const response = await apiClient.alerts.getEvents(page, DEFAULT_PAGE_SIZE);
        dispatch(AlertsActions.getEvents.done({params: loadState, result: response.results.data}));
      } catch (error) {
        dispatch(AlertsActions.getEvents.failed({params: loadState, error}));
      }
    };
  }
}
