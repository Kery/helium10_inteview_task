import {actionCreator} from "../../core/store/actionCreator";
import {LoadState} from "../../common/loadState";
import {IEvent} from "../../core/api/dto/AlertResponseDto";

export class AlertsActions {
  static getEvents = actionCreator.async<LoadState, IEvent[], Error>("Alerts/GET_EVENTS");
}
