import {Success} from "typescript-fsa";
import {reducerWithInitialState} from "typescript-fsa-reducers";
import {LoadState} from "../../common/loadState";
import {AlertsActions} from "./alertsActions";
import {alertsInitialState, IAlertsState} from "./IAlertsState";
import {IEvent} from "../../core/api/dto/AlertResponseDto";
import {newState} from "../../common/utils/newState";
import {combinePages} from "../../common/heplers/combinePages";

function getEventsStartedHandler(state: IAlertsState, params: LoadState): IAlertsState {
  return newState(state, {events: {...state.events, loadState: params}});
}

function getEventsDoneHandler(state: IAlertsState, success: Success<LoadState, IEvent[]>): IAlertsState {
  const {list, loadState} = combinePages(success.params, state.events.content, success.result);

  return newState(state, {events: {loadState, content: list}});
}

function getEventsFailedHandler(state: IAlertsState): IAlertsState {
  return newState(state, {events: {content: [], loadState: LoadState.error}});
}

export const alertsReducer = reducerWithInitialState(alertsInitialState)
  .case(AlertsActions.getEvents.started, getEventsStartedHandler)
  .case(AlertsActions.getEvents.done, getEventsDoneHandler)
  .case(AlertsActions.getEvents.failed, getEventsFailedHandler)
  .build();
